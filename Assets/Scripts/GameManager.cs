﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Advertisements;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int best;
    public int score;
    public int currentStage = 0;


    void Awake()
    {
        // IOS GameId
        Advertisement.Initialize("3171941");
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != null)
        {
            Destroy(gameObject);
        }

        best = PlayerPrefs.GetInt("Hightscore");
    }

    public void NextLevel()
    {
        currentStage++;
        FindObjectOfType<BallController>().ResetBallPosition();
        FindObjectOfType<HelixController>().LoadStage(currentStage);
    }

    public void RestartLevel()
    {

        // Show ads
        //AdvertisementSettings.S
        Advertisement.Show();
    //3171940
    score = 0;
        FindObjectOfType<BallController>().ResetBallPosition();
        FindObjectOfType<HelixController>().LoadStage(currentStage);
        //Reload stage
        //SceneManager.LoadScene(currentStage);
    }

    public void AddScore(int scoreToAdd)
    {
        score += scoreToAdd;

        if (score > best)
        {
            best = score;
            // Store hight score in playprefs
            PlayerPrefs.SetInt("Hightscore", score);
        }
    }
    
}
