﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelixController : MonoBehaviour
{

    private Vector2 lastTapPos;

    private Vector3 startRotation;

    public Transform topTransform;
    public Transform goalTransform;
    public GameObject helixLevelPrefab;

    public List<Stage> allStages = new List<Stage>();
    private float helixDistance;
    private List<GameObject> spawnedLevels = new List<GameObject>();

    private void Awake()
    {
        startRotation = transform.localEulerAngles;
        helixDistance = topTransform.localPosition.y - (goalTransform.localPosition.y +0.1f);
        LoadStage(0);

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 curTapPos = Input.mousePosition;
            if (lastTapPos == Vector2.zero)
            {
                lastTapPos = curTapPos;
            }

            float delta = lastTapPos.x - curTapPos.x;
            
            transform.Rotate(Vector3.up * delta);

            lastTapPos = curTapPos;
        }

        if (Input.GetMouseButtonUp(0))
        {
            lastTapPos =Vector2.zero;
            
        }
    }

    public void LoadStage(int stageNumber)
    {
        Stage stage = allStages[Mathf.Clamp(stageNumber, 0, allStages.Count - 1)];

        if (stage == null)
        {
            Debug.Log("No Stage " + stageNumber + " found in allStages list on Helix Controller");
        }
        // Change camera background color
        Camera.main.backgroundColor = allStages[stageNumber].stageBackgroundColor;
        // Change ball color
        FindObjectOfType<BallController>().GetComponent<Renderer>().material.color =
            allStages[stageNumber].stageBallColor;
        FindObjectOfType<BallController>().startColor = allStages[stageNumber].stageBallColor;

        transform.localEulerAngles = startRotation;

        // Destroy old levels
        foreach (GameObject go in spawnedLevels)
        {
            Destroy(go);
        }

        float levelDistance = helixDistance / stage.levels.Count;
        float spawnPosY = topTransform.localPosition.y;

        for (int i = 0; i < stage.levels.Count; i++)
        {
            spawnPosY -= levelDistance;
            //Create level with Scene
            GameObject level = Instantiate(helixLevelPrefab, transform);
            Debug.Log("Level Spawned");
            level.transform.localPosition = new Vector3(0, spawnPosY, 0);
            spawnedLevels.Add(level);
            int partToDisable = 12 - stage.levels[i].partCount;
            List<GameObject> disableParts = new List<GameObject>();

            //Creating gap with disable Part
            while (disableParts.Count < partToDisable)
            {
                GameObject randomPart =
                    level.transform.GetChild(Random.Range(0, level.transform.childCount)).gameObject;
                if (!disableParts.Contains(randomPart))
                {
                    randomPart.SetActive(false);
                    disableParts.Add(randomPart);
                }
            }

            //Color part left on level
            List<GameObject> leftParts = new List<GameObject>();
            foreach (Transform t in level.transform)
            {
                t.GetComponent<Renderer>().material.color = allStages[stageNumber].stageLevelPartColor;
                if (t.gameObject.activeInHierarchy)
                {
                    leftParts.Add(t.gameObject);
                }
            }

            //Create Death Part
            List<GameObject> deathPart = new List<GameObject>();
            while (deathPart.Count < stage.levels[i].deathPartCount)
            {
                GameObject randomPart = leftParts[(Random.Range(0, leftParts.Count))];
                if (!deathPart.Contains(randomPart))
                {
                    randomPart.gameObject.AddComponent<DeathPart>();
                    deathPart.Add(randomPart);
                }
            }

        }
    }
}
