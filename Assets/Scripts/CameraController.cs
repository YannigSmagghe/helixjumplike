﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public BallController target;
    private float offset;

    void Awake()
    {
        offset = transform.position.y - target.transform.position.y;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 currPos = transform.position;
        currPos.y = target.transform.position.y + offset;
        transform.position = currPos;
    }
}
