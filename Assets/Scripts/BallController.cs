﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float impusleForce = 5f;

    public int perfectPass = 0;
    public bool isSuperSpeedActive;

    private bool ignoreNextCollision;

    private Rigidbody rig;

    private Vector3 startPosition;
    [HideInInspector]
    public Color startColor;


    // Start is called before the first frame update
    void Awake()
    {
        rig = GetComponent<Rigidbody>();
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (perfectPass >= 3 && !isSuperSpeedActive)
        {
            isSuperSpeedActive = true;
            rig.AddForce(Vector3.down * 10,ForceMode.Impulse);
            SetColorSuperActive();
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        // Speed of my rig = velocity
        if (ignoreNextCollision)
            return;

        if (isSuperSpeedActive)
        {
            if (!collision.transform.GetComponent<Goal>())
            {
                Destroy(collision.transform.parent.gameObject,0.3f);
            }

            
        }
        else
        {
            DeathPart deathPart = collision.transform.GetComponent<DeathPart>();
            if (deathPart)
                deathPart.HitDeathPart();
        }

        rig.velocity = Vector3.zero;
        rig.AddForce(Vector3.up * impusleForce,ForceMode.Impulse);

        ignoreNextCollision = true;
        Invoke("AllowCollision",.2f);

        perfectPass = 0;
        isSuperSpeedActive = false;
        SetColorSuperActive();

    }

    private void SetColorSuperActive()
    {
        if (isSuperSpeedActive)
        {
            GetComponent<Renderer>().material.color = Color.red;
        }
        else
        {
            GetComponent<Renderer>().material.color = startColor;
        }
    }


    private void AllowCollision()
    {
        ignoreNextCollision = false;
    }

    public void ResetBallPosition()
    {
        transform.position = startPosition;
    }
}
